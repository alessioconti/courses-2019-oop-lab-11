﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
    class Frenchdeck
    {
        private Card[] cards;

        public Frenchdeck()
        {
            cards = new Card[Enum.GetNames(typeof(FrenchSeed)).Length * Enum.GetNames(typeof(FrenchValue)).Length + Enum.GetNames(typeof(Jolly)).Length];
        }

        public void Initialize()
        {
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */
            int i = 0;
            foreach (FrenchSeed seed in (FrenchSeed[])Enum.GetValues(typeof(FrenchSeed)))
            {
                foreach (FrenchValue value in (FrenchValue[])Enum.GetValues(typeof(FrenchValue)))
                {
                    cards[i] = new Card(value.ToString(), seed.ToString());
                    i++;
                }
            }
            cards[i] = new Card(1.ToString(), Jolly.JOLLY1.ToString());
            i++;
            cards[i] = new Card(2.ToString(), Jolly.JOLLY2.ToString());

            // throw new NotImplementedException();
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */
            foreach (Card c in cards)
            {
                Console.WriteLine(c.ToString());
            }

            // throw new NotImplementedException();
        }

        public Card this[Object seed, Object value]
        {
            get
            {
                foreach (Card c in cards)
                {
                    if (c.Seed.Equals(seed.ToString()) && c.Value.Equals(value.ToString()))
                    {
                        return c;
                    }
                }
                throw new Exception("Fat de in te cul.");
            }
        }
    }

    enum Jolly
    {
        JOLLY1,
        JOLLY2
    }

    enum FrenchSeed
    {
        PICCHE,
        QUADRI,
        FIORI,
        CUORI
    }

    enum FrenchValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        OTTO,
        NOVE,
        DIECI,
        J,
        Q,
        K
    }

}



    