﻿namespace Cards
{
    class Card
    {

        public Card(string value, string seed)
        {
            this.Value = value;
            this.Seed = seed;
        }

        public string Seed { get; private set; }

        public string Value { get; private set; }

        public override string ToString()
        {
            // TODO comprendere il meccanismo denominato in C# "string interpolation"
            return $"{this.GetType().Name}(Name={this.Value}, Seed={this.Seed})";
        }
    }

    
}
