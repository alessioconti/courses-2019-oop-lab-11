﻿using System;

namespace ComplexNumbers
{
    class ComplexNum
    {
        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public double Re { get; private set; }
        public double Im { get; private set; }


        // Restituisce il modulo del numero complesso
        public double Module
        {
            get
            {
                return Math.Sqrt(this.Re*this.Re + this.Im*this.Im);
            }
        }

        // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
        public ComplexNum Conjugate
        {
            get
            {
                return new ComplexNum(this.Re, 0-this.Im);
            }
        }

        // Restituisce l'inverso del numero complesso (https://it.wikipedia.org/wiki/Inverso_di_un_numero_complesso)
        public ComplexNum Invert
        {
            get
            {
                ComplexNum coniugate = new ComplexNum(this.Re, 0 - this.Im);
                double div = this.Re * this.Re + this.Im * this.Im;
                coniugate.Re = coniugate.Re / div;
                coniugate.Im = coniugate.Im / div;
                return coniugate;
            }
        }

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            String sign = (this.Im >= 0) ? "+" : "";
            return("Numero: " + this.Re + sign + this.Im + "i");
        }

        public static ComplexNum operator +(ComplexNum a, ComplexNum b)
        {
            return new ComplexNum(a.Re + b.Re, a.Im + b.Im);
        }

        public static ComplexNum operator -(ComplexNum a, ComplexNum b)
        {
            return new ComplexNum(a.Re - b.Re, a.Im - b.Im);
        }

        public static ComplexNum operator *(ComplexNum a, ComplexNum b)
        {
            return new ComplexNum((a.Re * b.Re) - (a.Im * b.Im), (a.Im * b.Re) + (a.Re * b.Im));
        }

        public static ComplexNum operator /(ComplexNum a, ComplexNum b)
        {
            return new ComplexNum(((a.Re * b.Re) + (a.Im * b.Im))/(b.Re*b.Re + b.Im*b.Im), ((a.Im * b.Re) - (a.Re * b.Im)) / (b.Re * b.Re + b.Im * b.Im));
        }
    }
}
